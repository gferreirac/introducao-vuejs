<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Models\Usuarios;

class UsuariosController extends Controller
{
    public function getUsuarios()
    {
        $usuarios = Usuarios::all();
        return json_encode($usuarios);
    }

    public function getUsuarioEdit($id)
    {
        $usuario = Usuarios::find($id);
        return json_encode($usuario);
    }

    public function salvarUsuario(Request $req)
    {
        if ($req->id) {
            $usuarios = Usuarios::where('id', '<>', $req->id)->get();
        } else {
            $usuarios = Usuarios::all();
        }

        $ano_nasc = substr($req->data_nasc, 0, 3);
        if ((int)$ano_nasc > 2003) {
            $result = [
                'error' => true,
                'message' => 'Usuário deve ser maior de idade!'
            ];
            return json_encode($result);
        }

        if (!strlen(strval($req->cpf)) == 11) {
            $result = [
                'error' => true,
                'message' => 'CPF incorreto!'
            ];
            return json_encode($result);
        }

        if (strlen(strval($req->fone)) < 10) {
            $result = [
                'error' => true,
                'message' => 'Número de Telefone incorreto!'
            ];
            return json_encode($result);
        }

        if (count($usuarios)) {
            foreach ($usuarios as $usuario) {
                if ($req->email == $usuario->email) {
                    $result = [
                        'error' => true,
                        'message' => 'E-mail já está cadastrado no sistema!'
                    ];
                    return json_encode($result);
                }
                if ($req->cpf == $usuario->cpf) {
                    $result = [
                        'error' => true,
                        'message' => 'CPF já está cadastrado no sistema!'
                    ];
                    return json_encode($result);
                }
            }
        }

        if ($req->id) {
            $usuario = Usuarios::find($req->id);

            DB::beginTransaction();
            try {
                $usuario->nome = $req->nome;
                $usuario->fone = $req->fone;
                $usuario->data_nasc = $req->data_nasc;
                $usuario->cpf = $req->cpf;
                $usuario->email = $req->email;

                $salvar = $usuario->save();
                if ($salvar) {
                    DB::commit();
                    $result = [
                        'error' => false,
                        'message' => 'Cadastro editado com sucesso!'
                    ];
                } else {
                    DB::rollback();
                    $result = [
                        'error' => true,
                        'message' => 'Erro ao editar cadastro!'
                    ];
                }
                return json_encode($result);
            } catch (\Exception $e) {
                DB::rollback();
                $result = [
                    'error' => true,
                    'message' => "Linha: " . $e->getLine() . ' | Mensagem: ' . $e->getMessage()
                ];
                return json_encode($result);
            }
        } else {
            DB::beginTransaction();
            try {
                $salvar = Usuarios::create($req->all());
                if ($salvar) {
                    DB::commit();
                    $result = [
                        'error' => false,
                        'message' => 'Cadastro feito com sucesso!'
                    ];
                } else {
                    DB::rollback();
                    $result = [
                        'error' => true,
                        'message' => 'Erro ao realizar cadastro!'
                    ];
                }
                return json_encode($result);
            } catch (\Exception $e) {
                DB::rollback();
                $result = [
                    'error' => true,
                    'message' => "Linha: " . $e->getLine() . ' | Mensagem: ' . $e->getMessage()
                ];
                return json_encode($result);
            }
        }
    }
}
