new Vue({
    el: "#app",
    data: {
        mostrar_div: false,
        showModalEditar: false,
        showModalDeletar: false,
        usuarios: [
            { id: 1, nome: 'Gleydson', cpf: '123.456.789-01', fone: '(85) 98919-3346', email: 'gleydson@teste.com' },
            { id: 2, nome: 'Vanessa', cpf: '987.654.321-98', fone: '(85) 99999-9999', email: 'vanessa@teste.com' },
            { id: 3, nome: 'Gerardo', cpf: '147.258.369-12', fone: '(85) 91111-1111', email: 'gerardo@teste.com' },
            { id: 4, nome: 'Marcos', cpf: '963.852.741-98', fone: '(85) 92222-2222', email: 'marcos@teste.com' },
            { id: 5, nome: 'Cristiano', cpf: '321.564.987-78', fone: '(85) 93333-3333', email: 'cristiano@teste.com' }
        ]
    },
    methods: {
        mostrar() {
            this.mostrar_div = true;
        },
        esconder() {
            this.mostrar_div = false;
        }
    }
});