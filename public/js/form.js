Vue.use(VueMask.VueMaskPlugin);
let vm = new Vue({
    el: "#app",

    data: {
        errors: [],
        nome: '',
        data_nasc: '',
        cpf: '',
        fone: '',
        email: '',
        mostrarResult: false,
        msg: '',
        hasError: false,
        animacao: null,
    },
    methods: {
        voltar() {
            window.location.href = '/';
        },

        checkForm: function (e) {
            this.errors = [];

            if (!this.nome) {
                this.errors.push('O nome é obrigatório. ');
            }
            if (!this.data_nasc) {
                this.errors.push('A data de nascimento é obrigatório. ');
            }
            if (!this.cpf) {
                this.errors.push('O CPF é obrigatório. ');
            }
            if (!this.fone) {
                this.errors.push('O número de telefone é obrigatório. ');
            }
            if (!this.email) {
                this.errors.push('O e-mail é obrigatório. ');
            } else if (!this.validEmail(this.email)) {
                this.errors.push('Utilize um e-mail válido.');
            }

            if (!this.errors.length) {
                let data = { nome: this.nome, data_nasc: this.data_nasc, cpf: this.cpf.replace(/[^0-9]/g, ''), fone: this.fone.replace(/[^0-9]/g, ''), email: this.email };
                let self = this;
                axios.post('/salvarUsuario', data).then(response => {
                    self.msg = response.data.message;
                    self.mostrarResult = true;
                    self.animacao = true;
                    setTimeout(() => {
                        self.animacao = false;
                    }, 3000);
                    setTimeout(() => {
                        self.mostrarResult = false;
                    }, 5000);
                    
                    if (response.data.error) {
                       self.hasError = true;
                    }
                })
            } else {
                this.mostrarResult = true;
            }
            e.preventDefault();
        },
        validEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    },
    mounted() {

    }
});