Vue.use(VueMask.VueMaskPlugin);
let vm = new Vue({
    el: "#app",

    data: {
        mostrar_div: false,
        usuarios: [],
        usuario_edit: [],
    },
    methods: {
        mostrar() {
            this.mostrar_div = true;
        },
        esconder() {
            this.mostrar_div = false;
        },
        novo() {
            window.location.href = '/form';
        },
        editar: async function (id) {
            this.modal = "";
            let self = this;
            self.usuario_edit = '';
            self.usuario_edit = await axios.get('/getUsuarioEdit/' + id).then(async response => {
                return response.data;
            });
            this.show_modal = true;
        },

        checkEditar: function (e){
            this.errors = [];

            if (!this.usuario_edit.nome) {
                this.errors.push('O nome é obrigatório. ');
            }
            if (!this.usuario_edit.data_nasc) {
                this.errors.push('A data de nascimento é obrigatório. ');
            }
            if (!this.usuario_edit.cpf) {
                this.errors.push('O CPF é obrigatório. ');
            }
            if (!this.usuario_edit.fone) {
                this.errors.push('O número de telefone é obrigatório. ');
            }
            if (!this.usuario_edit.email) {
                this.errors.push('O e-mail é obrigatório. ');
            } else if (!this.validEmail(this.usuario_edit.email)) {
                this.errors.push('Utilize um e-mail válido.');
            }
            if (!this.errors.length) {
                this.usuario_edit.cpf = this.usuario_edit.cpf.replace(/[^0-9]/g, '');
                this.usuario_edit.fone = this.usuario_edit.fone.replace(/[^0-9]/g, '');
                let data = this.usuario_edit;
                let self = this;
                axios.post('/salvarUsuario', data).then(response => {                    
                    if (response.data.error) {
                       self.hasError = true;
                    }else{
                        alert(response.data.message);                        
                    }
                })
            } else {
                this.mostrarResult = true;
            }
            e.preventDefault();
        },
        validEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    },
    created() {
        axios.get('/getUsuarios').then(response => {
            response.data.forEach(function (item) {
                let data = new Date(item.data_nasc);
                item.data_nasc = data.toLocaleDateString('pt-BR', { timeZone: 'UTC' });
                if (item.cpf.length == 11) {
                    item.cpf = item.cpf.toString().replace(/^(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
                }
                item.fone = item.fone.toString().replace(/^(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
            },
                this.usuarios = response.data
            );
        });
    }
});