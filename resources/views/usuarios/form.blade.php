@extends('index')
@section('styles')
@parent
<style type="text/css">
    .visible {
        visibility: visible;
        opacity: 1;
        transition: opacity 2s linear;
    }

    .hidden {
        visibility: hidden;
        opacity: 0;
        transition: visibility 0s 2s, opacity 2s linear;
    }
</style>
@endsection
@section('content')
@parent
<div id="app">
    <form @submit="checkForm">
        <div class="mt-4 col-md-10 offset-md-1 card">
            <div class="card-header bg-success">
                <h4 class="text-white"><i class="fa fa-plus fa-sm"></i> Novo usuário</h4>
            </div>
            <div class="card-body">
                <div class="alert static" v-bind:class="[hasError ? 'alert-danger' : 'alert-success', animacao ? 'visible' : 'hidden']" v-if="mostrarResult">
                    <p>@{{msg}}</p>
                    <ul v-for="erro in errors">
                        <li>@{{erro}}</li>
                    </ul>

                </div>
                <div class="row g-3">
                    <div class="col-md-12">
                        <label class="form-label" for="nome">Nome: </label>
                        <input class="form-control" type="text" id="nome" name="nome" v-model="nome">
                    </div>
                    <div class="col-md-4">
                        <label class="form-label" for="data_nasc">Data de Nascimento: </label>
                        <input class="form-control" type="date" id="data_nasc" name="data_nasc" v-model="data_nasc">
                    </div>
                    <div class="col-md-4">
                        <label class="form-label" for="cpf">CPF: </label>
                        <input class="form-control" type="text" id="cpf" name="cpf" v-model="cpf" v-mask="'###.###.###-##'">
                    </div>
                    <div class="col-md-4">
                        <label class="form-label" for="fone">Telefone: </label>
                        <input class="form-control" type="text" id="fone" name="fone" v-model="fone" v-mask="'(##) #####-####'">
                    </div>
                    <div class="col-md-12">
                        <label class="form-label" for="email">E-mail: </label>
                        <input class="form-control" type="email" id="email" name="email" v-model="email">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="d-grid d-md-flex gap-2 col-md-12 justify-content-center">
                    <button type="button" class="btn btn-danger col-md-4" @click="voltar"><i class="fa fa-arrow-left"></i> Voltar</button>
                    <button type="submit" class="btn btn-success col-md-4"><i class="fa fa-save"></i> Salvar Registro</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
@parent
<script src="{{URL::asset('js/form.js')}}"></script>
@endsection