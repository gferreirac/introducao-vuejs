@extends('index')
@section('content')
@parent
<div id="app">
    <div class="mt-4 col-md-10 offset-md-1 card">
        <div class="card-header bg-success">
            <h4 class="text-white"><i class="fa fa-file-text-o"></i> Usuários</h4>
        </div>
        <div id="div_form" class="card-body" v-if="mostrar_div">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead class="bg-light">
                        <tr>
                            <th>COD.</th>
                            <th>NOME</th>
                            <th>CPF</th>
                            <th>TELEFONE</th>
                            <th>DATA DE NASC.</th>
                            <th>E-MAIL</th>
                            <th>AÇÕES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="usuario of usuarios" :key="usuario.id">
                            <td>@{{usuario.id}}</td>
                            <td>@{{usuario.nome}}</td>
                            <td>@{{usuario.cpf}}</td>
                            <td>@{{usuario.fone}}</td>
                            <td>@{{usuario.data_nasc}}</td>
                            <td>@{{usuario.email}}</td>
                            <td class="col-md-1">
                                <div class="btn-group gap-1">
                                    <button class="btn btn-success" @click="editar(usuario.id)" title="Editar Registro" data-bs-toggle="modal" data-bs-target="#modal-editar"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-danger" @click="deletar(usuario.id)" title="Deletar Registro"><i class="fa fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer col-md-12">
            <div class="d-grid d-md-flex gap-2 col-md-12 justify-content-center">
                <button v-if="mostrar_div" type="button" class="btn btn-success col-md-4" title="Adicionar Registro" @click="novo"><i class="fa fa-plus"></i> Novo</button>

                <button v-if="!mostrar_div" type="button" class="btn btn-success col-md-4" v-on:click="mostrar"><i class="fa fa-eye"></i> Mostrar Tabela</button>
                <button v-if="mostrar_div" type="button" class="btn btn-danger col-md-4" v-on:click="esconder"><i class="fa fa-eye-slash"></i> Esconder Tabela</button>
            </div>
        </div>
    </div>

    <div id="modal-editar" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Usuário - @{{usuario_edit.nome}}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form @submit="checkEditar">
                    <div class="modal-body">
                        <input type="hidden" id="id-edit" name="id-edit" :value="usuario_edit.id">
                        <div class="row g-3">
                            <div class="col-md-12">
                                <label class="form-label">Nome:</label>
                                <input type="text" id="nome-edit" name="nome-edit" class="form-control" v-model="usuario_edit.nome">
                            </div>
                            <div class="col-md-4">
                                <label class="form-label">CPF:</label>
                                <input type="text" id="cpf-edit" name="cpf-edit" class="form-control" v-model="usuario_edit.cpf" v-mask="'###.###.###-##'">
                            </div>
                            <div class="col-md-4">
                                <label class="form-label">Telefone:</label>
                                <input type="text" id="fone-edit" name="fone-edit" class="form-control" v-model="usuario_edit.fone" v-mask="'(##) #####-####'">
                            </div>
                            <div class="col-md-4">
                                <label class="form-label">Data de Nascimento:</label>
                                <input type="date" id="data_nasc-edit" name="data_nasc-edit" class="form-control" v-model="usuario_edit.data_nasc">
                            </div>
                            <div class="col-md-12">
                                <label class="form-label">E-mail:</label>
                                <input type="email" id="email-edit" name="email-edit" class="form-control" v-model="usuario_edit.email">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>
                        <button type="submit" class="btn btn-success" data-bs-dismiss="modal"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script type="module" src="{{URL::asset('js/index.js')}}"></script>
@endsection