<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('usuarios.index');
});

Route::get('/form', function(){
    return view('usuarios.form');
});

Route::get('/getUsuarios', 'App\Http\Controllers\UsuariosController@getUsuarios');
Route::get('/getUsuarioEdit/{id}', 'App\Http\Controllers\UsuariosController@getUsuarioEdit');
Route::post('/salvarUsuario', 'App\Http\Controllers\UsuariosController@salvarUsuario');


